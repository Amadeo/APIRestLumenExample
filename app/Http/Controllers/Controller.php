<?php

namespace App\Http\Controllers;

use App\Models\Score;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function addPessoaAction($request){

        $newScore = new Score();
        $newScore->name = $request->input('name');
        $newScore->points = $request->input('points');

        if($newScore->save()){
            return response()->json($newScore);
        }else{
            return response()->json("Erro ao tentar gravar novo score");
        }
        
    }
}
