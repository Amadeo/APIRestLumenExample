<?php

use App\Models\Score;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

$app->get('/', function () use ($app) {
    return $app->version();
});
// teste commit git ssh key
// CRUD
// CREATE
$app->post('/scores/new_score', ['middleware' => 'auth', function (Request $request) use ($app) {

    $controller = new Controller();
    return $controller->addPessoaAction($request);
}]);

// READ
$app->get('/scores',  function () use ($app) {
    return response()->json(Score::all());
});

// UPDATE
$app->post('/scores/edit', ['middleware' => 'auth', function (Request $request) use ($app) {

    $findObject = Score::where('name','=',$request->input('name'))->first();

    if($findObject){

        $findObject->points = $request->input('points');
        $findObject->save();

        $response = response()->json($findObject);
    }else{
        $response = response()->json("Nome nao encontrado na base");
    }

    return $response;
}]);

// DELETE
$app->post('/scores/delete',['middleware' => 'auth', function (Request $request) use ($app) {

    $findObject = Score::where('id','=',$request->input('id'))->first();

    if($findObject){

        $findObject->delete();

        $response = response()->json("Registro deletado com sucesso");
    }else{
        $response = response()->json("Nome nao encontrado na base");
    }

    return $response;
}]);

