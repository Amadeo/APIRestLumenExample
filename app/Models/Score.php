<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string name
 * @property int points
 */
class Score extends Model
{
	public $table = "score";

	protected $guarded = ['id'];
}